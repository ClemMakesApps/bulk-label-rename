const axios = require('axios');

const token = 'TOKEN_NAME';
const oldLabelName = 'OLD_LABEL_NAME';
const newLabelName = 'NEW_LABEL_NAME';
const paginationLength = 100;

const API_URL = 'https://gitlab.com/api/v4';

const listIssuesUrl = `${API_URL}/groups/9970/issues?state=opened&labels=${oldLabelName}&per_page=${paginationLength}&private_token=${token}`;

const shouldAddNewLabel = true;
const shouldRemoveOldLabel = true;

axios.get(listIssuesUrl)
  .then(({ data }) => {

    data.forEach((issue) => {

      const existingIssueLabels = issue.labels;
      const hasNewLabel = existingIssueLabels.indexOf(newLabelName) != -1;

      // Add new label
      if (!hasNewLabel && shouldAddNewLabel) {
        const newLabels = [...existingIssueLabels, newLabelName].join(',');

        // API call to add new label
        axios.put(`${API_URL}/projects/${issue.project_id}/issues/${issue.iid}`, {
          labels: newLabels,
        }, {
          headers: {
            'PRIVATE-TOKEN': token,
          }
        }).catch(error => {
          console.log(error);
        });

        console.log(`Added new label ${newLabelName} for issue (#${issue.iid}) "${issue.title}"`)
      }

      // Would also have old label because of query
      if (hasNewLabel && shouldRemoveOldLabel) {
        const issueLabels = [...existingIssueLabels].filter(label => label !== oldLabelName)

        // Remove old label
        axios.put(`${API_URL}/projects/${issue.project_id}/issues/${issue.iid}`, {
          labels: issueLabels,
        }, {
          headers: {
            'PRIVATE-TOKEN': token,
          }
        }).catch(error => {
          console.log(error);
        });

        console.log(`Removed old label ${oldLabelName} from issue (#${issue.iid}) "${issue.title}"`)
      }

    });
  })
  .catch(error => {
    console.log(error);
  });
